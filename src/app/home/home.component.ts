import { Component, OnInit } from '@angular/core';
import {Article} from "../shared/models/article.model";
import {ArticleService} from "../shared/services/article.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  articles: Article[];

  constructor(private articleService: ArticleService) {}

  public ngOnInit():void {
    this.articleService.getAllArticles().subscribe(articles => this.articles = articles);
  }
}
