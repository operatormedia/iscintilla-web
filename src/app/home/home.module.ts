import { NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";
import { HomeComponent } from './home.component';
import { ArticleModule } from "../article/article.module";

@NgModule({
  imports: [
    ArticleModule,
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomeComponent
      }
    ])
  ],
  declarations: [
    HomeComponent
  ]
})
export class HomeModule { }
