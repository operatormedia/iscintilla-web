import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from "@angular/router";

import { AppComponent } from './app.component';

import { ApiService } from './shared/services/api.service';
import { JwtService } from './shared/services/jwt.service';
import { SharedModule } from './shared/shared.module';
import { ArticleService } from "./shared/services/article.service";
import { HeaderComponent } from "./shared/layout/header.component";
import { HomeModule } from "./home/home.module";
import { ArticleModule } from "./article/article.module";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    SharedModule,
    HttpClientModule,
    RouterModule.forRoot([], { useHash: false }),
    ArticleModule,
    HomeModule
  ],
  providers: [
    ApiService,
    ArticleService,
    JwtService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
