import {Component, Input, OnInit} from '@angular/core';
import {Article} from "../shared/models/article.model";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  @Input() article: Article;
  currentUrl: string;

  constructor(private route: ActivatedRoute) {
    this.currentUrl = window.location.href;
  }

  ngOnInit() {
    this.route.data.subscribe(
      (data: { article: Article }) => {
        if (data.article) {
          this.article = data.article;
        }
      }
    );
  }
}
