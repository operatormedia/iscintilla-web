import { Injectable } from '@angular/core';
import {ArticleService} from "../shared/services/article.service";
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from "@angular/router";
import {Article} from "../shared/models/article.model";
import {Observable} from "rxjs/Observable";

@Injectable()
export class ArticleResolverService implements Resolve<Article>{

  constructor(private articleService: ArticleService,
              private router: Router) { }


  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    return this.articleService.getArticle(route.params['slug']).catch(() => this.router.navigateByUrl('/'));
  }
}
