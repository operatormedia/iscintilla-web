import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleComponent } from "./article.component";
import { RouterModule } from "@angular/router";
import { ArticleResolverService } from "./article-resolver.service";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: 'news/:slug',
        component: ArticleComponent,
        resolve: {
          article: ArticleResolverService
        }
      }
    ])
  ],
  providers: [
    ArticleResolverService
  ],
  declarations: [
    ArticleComponent
  ],
  exports: [
    ArticleComponent
  ]
})
export class ArticleModule { }
