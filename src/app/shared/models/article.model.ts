export class Article {

  slug: string;
  text: string;
  title: string;
}
