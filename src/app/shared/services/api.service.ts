import { HttpHeaders, HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Rx';

import { environment } from '../../../environments/environment';

import { JwtService } from './jwt.service';

@Injectable()
export class ApiService {

  constructor(
    private http: HttpClient,
    private jwtService: JwtService
  ) {}

  private setHeaders(): HttpHeaders {
    const headersConfig = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };

    if (this.jwtService.getToken()) {
      headersConfig['Authorization'] = `Token ${this.jwtService.getToken()}`;
    }

    return new HttpHeaders(headersConfig);
  }

  private formatErrors(error: any) {
    return Observable.throw(error.json());
  }

  get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this.http.get(`${environment.api_url}${path}`, { headers: this.setHeaders(), params: params })
      .catch(this.formatErrors)
      .map(res => { return res; });
  }

  // put(path: string, body: Object = {}): Observable<any> {
  //   return this.http.put(
  //     `${environment.api_url}${path}`,
  //     JSON.stringify(body),
  //     { headers: this.setHeaders() }
  //   )
  //     .catch(this.formatErrors)
  //     .map((res: Response) => res.json());
  // }
  //
  // post(path: string, body: Object = {}): Observable<any> {
  //   return this.http.post(
  //     `${environment.api_url}${path}`,
  //     JSON.stringify(body),
  //     { headers: this.setHeaders() }
  //   )
  //     .catch(this.formatErrors)
  //     .map((res: Response) => res.json());
  // }
  //
  // delete(path): Observable<any> {
  //   return this.http.delete(
  //     `${environment.api_url}${path}`,
  //     { headers: this.setHeaders() }
  //   )
  //     .catch(this.formatErrors)
  //     .map((res: Response) => res.json());
  // }
}
