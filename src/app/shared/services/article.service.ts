import { Injectable } from '@angular/core';
import {Article} from "../models/article.model";
import {Observable} from "rxjs/Observable";
import {ApiService} from "./api.service";

@Injectable()
export class ArticleService {

  constructor(private apiService: ApiService) { }

  getAllArticles(): Observable<Article[]> {
    return this.apiService.get('/articles');
  }

  getArticle(slug: string): Observable<Article> {
    return this.apiService.get('/articles/' + slug);
  }
}
